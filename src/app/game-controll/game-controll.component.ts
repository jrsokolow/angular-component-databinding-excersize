import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-game-controll',
  templateUrl: './game-controll.component.html',
  styleUrls: ['./game-controll.component.css']
})
export class GameControllComponent implements OnInit {

  @Output() gameStarted = new EventEmitter<number>();
  counter = 0;
  interval;

  constructor() { }

  ngOnInit() {
  }

  startGame() {
    this.interval = setInterval(() => {
      this.gameStarted.emit(++this.counter);
    }, 1000);
  }

  stopGame() {
    clearInterval(this.interval);
  }

}
